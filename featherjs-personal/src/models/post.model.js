// post-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const post = new Schema(
    {
      title: { type: String, require: true },
      content: { type: String, require: true },
      article: { type: String, require: true },
      date: { type: Date, default: Date.now },
      image: { type: String, require: true }
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model('post', post);
};
