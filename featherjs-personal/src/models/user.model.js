// user-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function(app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const user = new Schema(
    {
      username: { type: String },
      password: { type: String, require: true }
    },
    {
      timestamps: true
    }
  );

  return mongooseClient.model('user', user);
};
