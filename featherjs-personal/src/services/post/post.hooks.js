module.exports = {
  before: {
    all: [],
    find: [
      hook => {
        if (hook.params.query['$sort']) {
          hook.params.query['$sort'] = JSON.parse(hook.params.query['$sort']);
        }
        console.log(hook.params.query);
        return hook;
      }
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
