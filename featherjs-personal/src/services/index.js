const user = require('./user/user.service.js');
const post = require('./post/post.service.js');
const uploadFiles = require('./upload-files/upload-files.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(user);
  app.configure(post);
  app.configure(uploadFiles);
};
