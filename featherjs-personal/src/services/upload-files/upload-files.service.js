// Initializes the `upload-files` service on path `/upload-files`
const createService = require('feathers-mongoose');
const createModel = require('../../models/upload-files.model');
const hooks = require('./upload-files.hooks');
const blobService = require('feathers-blob');
const fs = require('fs-blob-store');
const blobStorage = fs('./public/upload-files');
const multer = require('multer');
const multipartMiddleware = multer();

module.exports = function(app) {
  // const Model = createModel(app);
  // const paginate = app.get('paginate');

  // const options = {
  //   Model,
  //   paginate
  // };

  // Initialize our service with any options it requires
  app.use(
    '/upload-files',
    multipartMiddleware.single('uri'),
    function(req, res, next) {
      req.feathers.file = req.file;
      next();
    },
    blobService({
      Model: blobStorage
    })
  );

  // Get our initialized service so that we can register hooks
  const service = app.service('upload-files');

  service.hooks(hooks);
};
