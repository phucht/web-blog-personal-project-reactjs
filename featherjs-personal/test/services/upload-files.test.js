const assert = require('assert');
const app = require('../../src/app');

describe('\'upload-files\' service', () => {
  it('registered the service', () => {
    const service = app.service('upload-files');

    assert.ok(service, 'Registered the service');
  });
});
