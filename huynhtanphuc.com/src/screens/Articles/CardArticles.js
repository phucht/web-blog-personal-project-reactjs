import React from 'react';
import PropsTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Grid,
  Paper,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  Button
} from '@material-ui/core';
import bgImg from '../../assets/107.jpg';

const styles = {
  card: {
    width: 345
  },
  media: {
    objectFit: 'cover'
  },
  paper: {
    padding: 10,
    textAlign: 'center',
    color: 'gray'
  }
};

function CardArticle(props) {
  const { classes, data, img } = props;
  const { title, content, article, date, image } = data;
  return (
    <Grid item spacing={50}>
      <Card className={classes.card}>
        <CardMedia
          component="img"
          className={classes.media}
          image={img + image}
          height="140"
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="title">
            {title}
          </Typography>
          <Typography paragraph={true} component="p">
            {content.substring(0, 300)}
          </Typography>
        </CardContent>
        <CardActions>
          <Button>Read More</Button>
        </CardActions>
      </Card>
    </Grid>
  );
}

CardArticle.propsTypes = {
  classes: PropsTypes.object.isRequired
};

export default withStyles(styles)(CardArticle);
