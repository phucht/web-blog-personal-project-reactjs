import React, { Component } from 'react';
import { Button } from '@material-ui/core';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  icon: {
    margin: theme.spacing.unit * 2
  }
});
class CreateArticle extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Button style={{ margin: 10 }} href="/create-new-post">
        <i className="fas fa-plus" style={{ color: '#fff' }} />
      </Button>
    );
  }
}
export default CreateArticle;
