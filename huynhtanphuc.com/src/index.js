import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './components/App';
import { Provider } from 'mobx-react';
import Store from './Stores';
import registerServiceWorker from './registerServiceWorker';
import { injectGlobal } from 'emotion';
require('react-js-vector-icons/fonts');

injectGlobal`
  *, *::before, *::after {
    box-sizing: border-box;
  }
  ul {
    margin: 0;
  }
  html, body {
    margin: 0;
    padding: 0;
    width: 100%;
    min-width: 20rem;
    overflow-x: hidden;
  }
  body {    
    background: linear-gradient(#2c3e50 40%, #32678b);
  }
`;
ReactDOM.render(
  <BrowserRouter>
    <Provider {...Store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);
registerServiceWorker();
