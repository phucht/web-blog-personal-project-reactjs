import io from 'socket.io-client';
import feathers from 'feathers/client';
import socketio from 'feathers-socketio/client';
import authentication from 'feathers-authentication-client';
import hooks from 'feathers-hooks';
import { observable } from 'mobx';
const localStorage = require('localstorage-memory');

const client = io('http://localhost:3030', {
  transports: ['websocket'],
  forceNew: true
});

const client = feathers()
  .configure(hooks())
  .configure(socketio(socket))
  .configure(authentication({ storage: localStorage }));

const messageService = client.service('message');
messageService.on('created', message =>
  console.log('Created a message', message)
);

messageService.create({
  text: 'Message from client'
});
