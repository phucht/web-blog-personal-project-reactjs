import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { css } from 'emotion';
import { LanguageContext } from './LanguageContext';
import { color } from './Theme';
import Icon from 'react-js-vector-icons';

const main = css({
  label: 'menu',
  display: 'flex',
  width: '100%',
  fontWeight: '600',
  padding: '0.07em',
  backgroundImage:
    'linear-gradient(90deg, #F95151 0%, #F9E35F 24%, #83D45A 49%, #62AFDF 74%, #8678DD 100%)',
  boxShadow: '0 0 0.1em 0 #000',
  zIndex: '10',
  ul: {
    display: 'flex',
    justifyContent: 'space-evenly',
    listStyleType: 'none',
    padding: 0,
    background: '#2d4a60',
    width: '100%'
  },

  'ul li': {
    alignSelf: 'center',
    textAlign: 'center',
    margin: '0',
    padding: '0.4em 0',
    width: '100%',
    fontFamily: 'FontAwesome',
    fontWeight: 'normal',
    fontSize: '3em'
  },
  a: {
    display: 'flex',
    flexDirection: 'column',
    span: {
      fontFamily: "'Poiret One', 'sans-serif'",
      fontSize: '0.28em',
      padding: '0.8em 0'
    }
  },

  'ul li a': {
    textDecoration: 'none',
    color: '#FBFCFF',
    transition: '0.1s all linear',
    lineHeight: '0'
  },

  'li a:hover': {
    color: color
  },
  'li a i:hover': {
    color: color
  },

  '.active': {
    color: color,
    transform: 'scale(1.15)'
  },

  '@media all and (min-width: 1280px)': {
    position: 'fixed',
    flexDirection: 'column',
    width: '10vw',
    minWidth: '10vw',
    fontSize: '1.3em',
    backgroundImage:
      'linear-gradient(180deg, #F95151 0%, #F9E35F 24%, #83D45A 49%, #62AFDF 74%, #8678DD 100%)',
    marginLeft: '1px',

    ul: {
      flexDirection: 'column',
      height: 'calc(100vh - 4px)'
    },

    li: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },

    'ul li': {
      height: '100vh'
    }
  }
});

class Menu extends Component {
  render() {
    return (
      <LanguageContext.Consumer>
        {({ langText }) => (
          <nav className={main}>
            <ul>
              <li>
                <NavLink to="/" exact activeClassName="active">
                  <Icon
                    color="null"
                    name="home"
                    font="Entypo"
                    size={60}
                    style={{ margin: 35 }}
                  />
                  <span>{langText.menu.home}</span>
                </NavLink>
              </li>
              <li>
                <NavLink to="/experience" exact activeClassName="active">
                  <Icon
                    color="null"
                    name="tools"
                    font="Entypo"
                    size={80}
                    style={{ margin: 35 }}
                  />
                  <span>{langText.menu.experience}</span>
                </NavLink>
              </li>
              <li>
                <NavLink to="/contact" exact activeClassName="active">
                  <Icon
                    color="null"
                    name="video"
                    font="Entypo"
                    size={80}
                    style={{ margin: 35 }}
                  />
                  <span>{langText.menu.contact}</span>
                </NavLink>
              </li>
              <li>
                <NavLink to="/article" exact activeClassName="active">
                  <Icon
                    color="null"
                    name="book"
                    font="Entypo"
                    size={80}
                    style={{ margin: 35 }}
                  />
                  <span>{langText.menu.projects}</span>
                </NavLink>
              </li>
            </ul>
          </nav>
        )}
      </LanguageContext.Consumer>
    );
  }
}

export default Menu;
