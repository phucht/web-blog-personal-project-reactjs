import React from 'react';
import { css } from 'emotion';
import LanguagePicker from './LanguagePicker';
import Icon from 'react-js-vector-icons';

const main = css({
  label: 'footer',
  display: 'flex',
  justifyContent: 'space-evenly',
  alignItems: 'center',
  color: '#fff',
  textAlign: 'end',
  fontSize: '0.8em',
  borderTop: '1px soild #3c759b',

  p: {
    fontSize: '1em',
    fontWeight: 'bold'
  },
  a: {
    textDecoration: 'none',
    color: '#fff'
  },
  'a:hover': {
    color: '#fff'
  },
  '.icon': {
    height: '1.5em',
    marginLeft: '1.5em'
  },

  '@media all and (min-width: 2500px)': {
    fontSize: '1.5em'
  },
  '@media all and (min-width: 1690px) and (max-width: 2499px)': {
    fontSize: '1em'
  },
  '@media all and (min-width: 1280px) and (max-width: 1689px)': {
    height: '5vh',
    fontSize: '1em',
    marginLeft: '10vw',

    '.icon': {
      marginLeft: '1.5em'
    }
  }
});

const Footer = () => (
  <footer className={main}>
    <section className="social">
      <a
        href="https://www.facebook.com/huynh.phuc.7121"
        target="_blank"
        rel="noopener noreferrer"
      >
        <Icon
          color="null"
          name="facebook"
          font="Entypo"
          size={30}
          style={{ height: 1.5, marginVertical: 25 }}
        />
      </a>

      <a
        href="https://github.com/vnhtpvn"
        target="_blank"
        rel="noopener noreferrer"
      >
        <Icon
          style={{ height: 1.5, marginLeft: 10 }}
          color="null"
          name="youtube"
          font="Entypo"
          size={30}
          // style={{ marginLeft: 5 }}
        />
      </a>

      <a
        href="https://www.linkedin.com/in/hu%E1%BB%B3nh-ph%C3%BAc-396066140/"
        target="_blank"
        rel="noopener noreferrer"
      >
        <Icon
          color="null"
          name="linkedin"
          font="Entypo"
          size={30}
          style={{ height: 1.5, marginLeft: 10 }}
        />
      </a>

      <a
        href="mailto:vn.htp.vn@gmail.com"
        target="_top"
        rel="noopener noreferrer"
      >
        <Icon
          color="null"
          name="mail"
          font="Entypo"
          size={30}
          style={{ height: 1.5, marginLeft: 10 }}
        />
      </a>
    </section>
    <LanguagePicker />
    <p>&copy;HTP {new Date().getFullYear()}</p>
  </footer>
);
export default Footer;
