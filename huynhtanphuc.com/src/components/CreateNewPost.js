import React, { Component } from 'react';
import PropsTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Input } from '@material-ui/core';
import { color } from './Theme';
import { observer, inject } from 'mobx-react';
import { action } from 'mobx';
import { observable } from 'mobx';

@inject('AppStore')
@observer
class CreateNewPost extends Component {
  @observable
  isUpdate = false;
  @observable
  img = null;
  @observable
  msgTitle = '';
  @observable
  msgContent = '';
  @observable
  msgArticle = '';
  @observable
  msgDate = '';
  @observable
  msgImage = '';

  componentDidMount() {
    this.props.AppStore.createPost;
  }

  @action
  handleChangeTitle = title => {
    this.props.AppStore.createPost.title = title.target.value;
  };
  @action
  handleChangeContent = content => {
    this.props.AppStore.createPost.content = content.target.value;
  };
  @action
  handleChangeArticle = article => {
    this.props.AppStore.createPost.article = article.target.value;
  };
  @action
  handleChangeDate = date => {
    this.props.AppStore.createPost.date = date.target.value;
  };

  @action
  handleChangeFile = e => {
    let img = e.target.files[0];
    var reader = new FileReader();
    reader.addEventListener('load', () => (this.img = reader.result), false);

    img && reader.readAsDataURL(img);
  };

  handleClickFileUpload = async () => {
    console.log('asda demo');
    let img = await this.props.AppStore.uploadImage(this.img);
    if (img) {
      this.props.AppStore.createPost.image = img;
      this.props.AppStore.savePost();
    } else {
      console.log('err');
    }
  };

  render() {
    return (
      <div style={{ width: '100%' }}>
        <div style={{ width: '100%', alignItems: 'center' }}>
          <h1>Create New Post</h1>
        </div>
        <div>
          <Input
            fullWidth
            placeholder="Title"
            item
            value={this.props.AppStore.createPost.title}
            onChange={this.handleChangeTitle}
            xs="12"
            style={{ backgroundColor: '#fff' }}
          />
          <Input
            fullWidth
            multiline="true"
            style={{ backgroundColor: '#fff', height: 300 }}
            placeholder="Content"
            value={this.props.AppStore.createPost.content}
            onChange={this.handleChangeContent}
            item
            xs="12"
          />
          <Input
            fullWidth
            placeholder="Article"
            value={this.props.AppStore.createPost.article}
            onChange={this.handleChangeArticle}
            item
            xs="12"
            style={{ backgroundColor: '#fff' }}
          />
          <div fullWidth>
            <Input
              type="date"
              placeholder="Date"
              style={{ backgroundColor: '#fff' }}
              value={this.props.AppStore.createPost.date}
              onChange={this.handleChangeDate}
            />
          </div>
          <div fullWidth>
            <input
              accept="image/*"
              type="file"
              style={{ backgroundColor: 'transparent', color: '#fff' }}
              onChange={this.handleChangeFile}
            />
          </div>
        </div>
        <div fullWidth style={{ alignItems: 'center' }}>
          <Button
            style={{
              width: '50%',
              backgroundColor: color,
              alignItems: 'center',
              color: '#000'
            }}
            onClick={() => {
              this.handleClickFileUpload();
              console.log('1231234');
            }}
            href="/article"
          >
            Submit
          </Button>
        </div>
      </div>
    );
  }
}
export default CreateNewPost;
