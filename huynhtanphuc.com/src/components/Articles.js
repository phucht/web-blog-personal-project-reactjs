import React, { Component } from 'react';
import { css } from 'emotion';
import CardArticles from '../screens/Articles/CardArticles';
import ButtonCreate from '../screens/Articles/btnCreateArticle';
import me from '../assets/logo.svg';
import { Grid, Paper } from '@material-ui/core';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx';

const main = css({
  label: 'samples',

  '.box': {
    display: 'flex',
    width: '100%',
    padding: '0.07em',
    backgroundImage: 'linear-gradient(90deg, #F95151, #8678DD)',
    boxShadow: '0 0 0.1em 0 #000000',
    zIndex: '10'
  },

  ul: {
    display: 'flex',
    justifyContent: 'space-evenly',
    listStyleType: 'none',
    padding: '0',
    width: '100%',
    flexWrap: 'wrap'
  },

  'ul li': {
    alignSelf: 'center',
    textAlign: 'center',
    margin: '0',
    padding: '1em 0',
    width: '33.1%',
    cursor: 'pointer',

    ':nth-child(odd)': {
      background: '#141E30'
    },

    ':nth-child(even)': {
      background: '#202d44'
    }
  }
});

@inject('AppStore')
@observer
class Article extends Component {
  componentDidMount() {
    this.props.AppStore.getPosts(6, 0);
  }
  render() {
    const urlImg = this.props.AppStore.api.getBaseUrl() + '/upload-files/';
    return (
      <div>
        <div>
          <ButtonCreate />
        </div>
        <div id="samples" className={main}>
          <Grid container spacing={8}>
            {this.props.AppStore.posts.map(item => {
              return <CardArticles data={item} img={urlImg} />;
            })}
          </Grid>
        </div>
      </div>
    );
  }
}
export default Article;
