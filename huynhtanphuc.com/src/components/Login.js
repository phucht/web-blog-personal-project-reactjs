import React, { Component } from 'react';
import { TextField, Paper, FlatButton } from 'material-ui';
const style = {
  width: 350,
  height: 400,
  margin: '0 auto',
  marginTop: 100,
  textAlign: 'center'
};
const headerStyle = {
  height: 56,
  backgroundColor: '#d3d3d3',
  textAlign: 'center',
  paddingTop: 20
};

class Login extends Component {
  render() {
    return (
      <div>
        <Paper style={style}>
          <div style={headerStyle}>Login</div>
          <div>
            <TextField placeholder="username" />
            <TextField placeholder="password" type="password" />
          </div>
        </Paper>
      </div>
    );
  }
}
export default Login;
