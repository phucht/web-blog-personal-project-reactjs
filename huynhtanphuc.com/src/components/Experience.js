import React, { Component } from 'react';
import { css } from 'emotion';
import { LanguageContext } from './LanguageContext';
import { color, yellow } from './Theme';
import Skill from './Skill';

const main = css({
  label: 'experience',
  display: 'flex',
  flexDirection: 'column',
  h2: {
    textAlign: 'center',
    color: 'color',
    margin: '1.5em 0 2em 0',
    letterSpacing: '0.2em'
  },
  'h2::before': {
    content: "''",
    display: 'block',
    width: '3em',
    borderTop: 'solid 1px',
    margin: '4em auto 1.5em auto'
  },
  blockquote: {
    fontSize: '0.9em',
    width: '100%',
    margin: '1em auto',
    fontStyle: 'italic',
    fontWeight: 'bold',
    padding: '0.5em 1.2em 0',
    borderLeft: `2px solid ${color}`,
    position: 'relative'
  },
  'blockquote::before': {
    fontFamily: 'Arial',
    content: "'\\201C'",
    color: color,
    fontSize: '2.3em',
    position: 'absolute',
    left: '2px',
    top: '0px'
  },

  'blockquote span': {
    display: 'block',
    fontStyle: 'normal',
    marginTop: '1em',
    fontWeight: 'normal'
  },

  ul: {
    listStyleType: 'none',
    padding: '0'
  },
  '.stack': {
    borderTop: `1px dashed ${yellow}`,
    borderBottom: `1px dashed ${yellow}`,
    boxShadow: `inset 0 -1px 0 0 ${yellow}, inset 0 1px 0 0 ${yellow}, 0 1px 0 0 ${yellow}, 0 -1px 0 0 ${yellow}`,
    padding: '0.5em 0',
    margin: '1.5em auto',
    background: '#0000002b'
  },

  '.stack span': {
    fontSize: '0.8em',
    margin: '0 0 0 1.5em',
    padding: '0'
  },

  '@media all and (min-width: 1690px)': {
    '.stack': {
      width: '65%'
    }
  },

  '@media all and (min-width: 1280px) and (max-width: 1689px)': {
    h2: {
      margin: '3em 0 1em 0',
      fontSize: '2em',
      letterSpacing: '0.2em'
    },

    '.stack': {
      width: '65%'
    }
  },

  '@media all and (min-width: 736px) and (max-width: 1279px)': {
    '.stack': {
      width: '65%'
    }
  }
});

class Experience extends Component {
  render() {
    const beSkill = [
      {
        title: 'FeathersJS',
        stars: ['on', 'on', 'off', 'off', 'off']
      },
      {
        title: 'NodeJS',
        stars: ['on', 'off', 'off', 'off', 'off']
      },
      {
        title: 'MongoDB',
        stars: ['on', 'off', 'off', 'off', 'off']
      },

      {
        title: 'React-Native',
        stars: ['on', 'on', 'on', 'off', 'off']
      }
    ];
    const feSkill = [
      {
        title: 'HTML 5',
        stars: ['on', 'on', 'on', 'on', 'off']
      },
      {
        title: 'CSS 3',
        stars: ['on', 'on', 'on', 'off', 'off']
      },
      {
        title: 'ReactJS',
        stars: ['on', 'on', 'on', 'off', 'off']
      }
    ];

    return (
      <section id="experience" className={main}>
        <LanguageContext.Consumer>
          {({ langText }) => (
            <section className="experience-content">
              <blockquote>
                Every great developer you know got there by solving problems
                they were unqualified to solve until they actually did it.
              </blockquote>

              <h2 className="backend">Back-End</h2>

              <section className="stack">
                <Skill skills={beSkill} side="be" />
              </section>

              <h2 className="frontend">Front-End</h2>

              <section className="stack">
                <Skill skills={feSkill} side="be" />
              </section>
            </section>
          )}
        </LanguageContext.Consumer>
      </section>
    );
  }
}
export default Experience;
