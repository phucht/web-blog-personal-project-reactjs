import apisauce from 'apisauce';

const create = (baseURL = 'http://localhost:3030') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  });

  const getBaseUrl = () => baseURL;

  const getAccount = username => api.get('user', { username });
  const getPosts = (limit, skip) =>
    api.get('post', { $limit: limit, $skip: skip, $sort: { createdAt: -1 } });

  const createPosts = (title, content, article, date, image) =>
    api.post('post', {
      title,
      content,
      article,
      date,
      image
    });

  const uploadImg = uri =>
    api.post('upload-files', {
      uri
    });

  return {
    getAccount,
    getPosts,
    createPosts,
    uploadImg,
    getBaseUrl
  };
};
export default {
  create
};
