import { observable, action, runInAction, computed } from 'mobx';
import io from 'socket.io-client';
import { autobind } from 'core-decorators';
import _ from 'lodash';
import feathers from 'feathers/client';
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client';
import authentication from 'feathers-authentication-client';
import API from '../Services/Api';
import { fail } from 'assert';
const localStogare = require('localstorage-memory');

let instance = false;
const socket = io('http://localhost:3030', {
  transports: ['websocket'],
  forceNew: true
});
var client = feathers()
  .configure(socketio(socket))
  .configure(hooks())
  .configure(authentication({ storage: localStogare }));

export default class Store {
  @observable
  name: string;
  @observable
  isLoading = false;
  @observable
  api = {};
  @observable
  posts = [];
  @observable
  createPost = { title: '', content: '', article: '', date: '', image: '' };
  @observable
  url = null;

  constructor() {
    console.log(API);
    this.api = API.create();
  }

  onChangeText(title) {
    this.createPost.title = title;
  }
  onChangeText(content) {
    this.createPost.content = content;
  }
  onChangeText(article) {
    this.createPost.article = article;
  }
  onChangeText(date) {
    this.createPost.date = date;
  }

  onChangeText(url) {
    this.createPost.image = url;
  }

  @action
  getAcc = async username => {
    try {
      let { data, ok, problem } = await this.api.getAccount(username);
      if (ok) {
        return data;
      }
      console.log(problem);
      return false;
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  @action
  getPosts = async ($limit, $skip) => {
    try {
      let { data, ok, problem } = await this.api.getPosts($limit, $skip);
      console.log('data', data, ok, problem);
      if (ok) {
        this.posts = data.data;
        return data;
      }
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  @action
  savePost = async () => {
    try {
      let { data, ok, problem } = await this.api.createPosts(
        this.createPost.title,
        this.createPost.content,
        this.createPost.article,
        this.createPost.date,
        this.createPost.image
      );

      if (ok) {
        console.log('tesstImage', data, ok, problem);
        this.isLoading = true;
      }
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  @action
  uploadImage = async uri => {
    try {
      let result = await this.api.uploadImg(uri);
      let { data, ok, problem } = result;
      console.log('ok', result);

      if (ok) {
        this.url = data.uri;
        console.log('ok', data, ok, problem);
        return data.id;
      }
      return false;
    } catch (error) {
      console.log(error);
      return false;
    }
  };
}
